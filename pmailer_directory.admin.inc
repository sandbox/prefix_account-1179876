<?php

/*
 * @file
 * pMailer Directory Module Admin Settings
 *
 * implementation of hook_admin_settings
 * @return <type>
 */

function pmailer_directory_admin_settings()
{

  $form['pmailer_directory_account_info'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => TRUE,
    '#title' => 'Insert pMailer API Information',
  );

  $form['pmailer_directory_account_info']['pmailer_directory_server'] = array(
    '#type' => 'textfield',
    '#title' => t('pMailer server'),
    '#required' => TRUE,
    '#default_value' => variable_get('pmailer_directory_server', ''),
    '#description' => t('Specify pmailer server. '),
  );

  $form['pmailer_directory_account_info']['pmailer_directory_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('pMailer API key'),
    '#required' => TRUE,
    '#default_value' => variable_get('pmailer_directory_api_key', ''),
    '#description' => t('The API key for your pmailer account. '),
  );

  $api_key = variable_get('pmailer_directory_api_key', FALSE);
  $server = variable_get('pmailer_directory_server', FALSE);


    $form['result_counter'] = array(
      '#type' => 'select',
      '#title' => t('Specify the number of items in list.'),
      '#default_value' => variable_get('result_counter',5),
      '#options' => array(
        '5 items'=>5,
        '10 items' => 10,   
        '20 items' => 20),
      '#description' => t('Setting to specify the number of previous entries to display on each page of the pMailer directory listing.'),
    );
 
  $form['#validate'][] = 'pmailer_directory_admin_settings_validate';
  return system_settings_form($form);
}

/**
 * validate the admin settings 
 *
 * @param <type> $form
 * @param <type> $form_state
 */
function pmailer_directory_admin_settings_validate($form, &$form_state)
{
  
}